---
- name: Gather variables for each operating system
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution | lower }}-{{ ansible_distribution_major_version | lower }}.yml"
    - "{{ ansible_distribution | lower }}.yml"
    - "{{ ansible_os_family | lower }}.yml"

- name: Ensure system runs on systemd when installation is enabled
  assert:
    that: not (boinc_installation_enabled | bool) or ansible_service_mgr == 'systemd'
    fail_msg: >-
      Installation is only supported on systemd

- name: Ensure RPC is enabled when remote host IPs are set
  assert:
    that: boinc_rpc_enabled | bool or boinc_remote_host_ips == []
    fail_msg: >-
      Cannot set remote host IPs when RPC is disabled.

- name: Ensure that percentage of max number of CPUs is within limits
  assert:
    that:
      - boinc_max_ncpus_pct | float > 0.0
      - boinc_max_ncpus_pct | float <= 100.0
    fail_msg: >-
      Variable 'boinc_max_ncpus_pct' cannot be set to value '{{ boinc_max_ncpus_pct }}'

- name: Ensure that percentage of CPU usage is within limits
  assert:
    that:
      - boinc_cpu_usage_limit | float > 0.0
      - boinc_cpu_usage_limit | float <= 100.0
    fail_msg: >-
      Variable 'boinc_cpu_usage_limit' cannot be set to value '{{ boinc_cpu_usage_limit }}'

- name: Ensure that work buffer values are sane
  assert:
    that:
      - boinc_work_buf_min_days | float >= 0.0
      - boinc_work_buf_additional_days | float >= 0.0
    fail_msg: >-
      Variables 'boinc_work_buf_min_days' and 'boinc_work_buf_additional_days' cannot be negative.

- name: Ensure boinc_projects is a list
  assert:
    that:
      - boinc_projects is iterable
      - not(boinc_projects is mapping)
      - not(boinc_projects is string)
    fail_msg: >-
      Variable 'boinc_projects' is not a list.

- name: Ensure project list items are sane
  no_log: true
  loop: "{{ boinc_projects }}"
  assert:
    that:
      - item is mapping
      - item.name is defined
      - item.url is defined
      - item.key is defined
    fail_msg: >-
      One of the objects of list 'boinc_projects' does not contain a 'name', 'url' or 'key' attribute.
