---
- name: Attach project {{ project.name }}
  when:
    - project.state | default('present') == 'present'
    - boinc_configuration_enabled | bool
  become: true
  # Contains project keys
  no_log: true
  template:
    src: "projects/{{ project.file | default('account_generic.xml') }}.j2"
    dest: "{{ boinc_data_dir }}/{{ project.file }}"
    owner: "{{ boinc_user_name }}"
    group: "{{ boinc_group_name }}"
    mode: '0644'
    force: false
  notify:
    - Restart boinc-client

- name: Detach project {{ project.name }}
  when:
    - not (boinc_configuration_enabled | bool)
      or (project.state | default('present') != 'present')
  become: true
  file:
    dest: "{{ boinc_data_dir }}/{{ project.file }}"
    state: absent
  notify:
    - Restart boinc-client

- name: Are we fetching new work?
  when: boinc_configuration_enabled | bool
  become: true
  shell:
    cmd: >
      {{ boinc_cmd_path }} --get_project_status |
      grep -A 18 "master URL: {{ project.url }}"
  check_mode: false
  changed_when: false
  failed_when: not (boinc_fetching_work.rc in [0, 1])
  ignore_errors: '{{ ansible_check_mode }}'
  register: boinc_fetching_work

- name: Stop fetching new work for {{ project.name }}
  when:
    - boinc_configuration_enabled | bool
    - project.state | default('present') == 'present'
    - '( "t request more work: no" in boinc_fetching_work.stdout )'
    - not (boinc_fetch_new_work | bool and (project.fetch_work | default(true) | bool))
  become: true
  command:
    chdir: "{{ boinc_data_dir }}"
    cmd: >
      {{ boinc_cmd_path }} --project {{ project.url | quote }} nomorework
  ignore_errors: '{{ ansible_check_mode }}'

- name: Start fetching new work for {{ project.name }}
  when:
    - boinc_configuration_enabled | bool
    - project.state | default('present') == 'present'
    - '( "t request more work: yes" in boinc_fetching_work.stdout )'
    - boinc_fetch_new_work | bool and (project.fetch_work | default(true) | bool)
  become: true
  command:
    chdir: "{{ boinc_data_dir }}"
    cmd: >
      {{ boinc_cmd_path }} --project {{ project.url | quote }} allowmorework
  ignore_errors: '{{ ansible_check_mode }}'
