# Ansible Role: ORDAA/ansible_boinc

## Description

Ansible role that deploys, configures or uninstalls the [Boinc distributed computing](https://boinc.berkeley.edu/) client program. It also enables attaching to Boinc projects and controlling work-unit intake.

## Requirements

- Ansible (Tested on version 2.13 but should work on earlier versions too, including 2.7)
- Tested on Debian 10 targets, but should work on any systemd Linux distribution. PRs welcome. See [vars/](vars/) for setting distribution specific options.

## Role Variables

All variables which can be overridden (in host_vars, group_vars or the command-line) are stored and documented (for the most part) in [defaults/main.yml](defaults/main.yml) file.

## Role tags

All role tags are specified in the role's [tasks/main.yml](tasks/main.yml) file.

- The **boinc** tag just runs the entire role.
- The **boinc_install** tag either installs or uninstalls the boinc client from the target system(s), depending of the value of the **boinc_installation_enabled** role variable.
- The **boinc_configure** tag just configures the boinc client on a target system where it is already installed. It can also be used with the ansible-playbook *--check-mode* and *--diff* parameters in order to check if there are any configuration differences with what's deployed.
- The generic **install** and **configure** tags allow this role to be part of a group of other roles that also support these tags so they can be installed or configured in a single step.

## Example

### Playbook

```yaml
---
- hosts:
    - all
  roles:
    - ansible_boinc
  vars:
    boinc_max_ncpus_pct: 80
    boinc_cpu_usage_limit: 25
    boinc_work_buf_min_days: 1.0
    boinc_projects:
      - name: 'LHC@home 1.0'
        url: "https://lhcathome.cern.ch/lhcathome/"
        key: "< your key here >"
        file: "account_lhcathome.cern.ch_lhcathome.xml"
        fetch_work: true
```

## ansible-lint

ansible-lint only complains about not using FQCN (fqcn-builtins: Use FQCN for builtin actions).
This is a deliberate choice for ansible compatibility reasons and will likely be fixed in the future.

## Links

If you use this role to deploy many Boinc client instances and you use Prometheus for monitoring, you may want to take a look into [boinc_exporter](https://gitlab.com/ordaa/boinc_exporter).

## Credits

This role was initially forked from Albert Mikaelyan's [ansible-role-boinc-client](https://github.com/Tahvok/ansible-role-boinc-client) role. Thank you Albert for the inspiration!

## License
[GPL version 3](https://www.gnu.org/licenses/)
You can find a full copy of the license in the file [COPYING](COPYING).

## Copyright
Thomas Venieris 2022, all rights reserved.
